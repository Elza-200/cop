// import axios from 'axios';

// axios.get('http://localhost:1337/').then(response => {
//   console.log(response);
// });


// --------------------------------------------------------------

const express = require('express');
const expressGraphQL = require('express-graphql');
const schema = require('./schema') 
const cors = require('cors')

const app = express();
const port = 4000;

app.use(cors());

app.use('/graphql', expressGraphQL ({
    graphql: true,
    schema: schema

}))

app.listen( 4000, () => {
    console.log("listening on :" + port);
});

// --------------------------------------------------------------

