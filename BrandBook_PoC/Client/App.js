
import React from 'react';
import { Button, StyleSheet, TextInput, View, Text, Pressable } from 'react-native';
import { Register}  from './src/screens/Register';
import { GetProfile } from './src/screens/GetProfile';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';

const client = new ApolloClient({ uri: 'http://localhost:1337/graphql' });
const Stack = createStackNavigator();


export function app () {
      return (
        <ApolloProvider client={client}>
          <NavigationContainer>
              <Stack.Navigator>
                <Stack.Screen name="Register New Account" component={Register}/> 
                <Stack.Screen name="GetProfile" component={GetProfile}/>
              </Stack.Navigator>
          </NavigationContainer>
         </ApolloProvider>
      
      )   
  }


 const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  
  inputStyles: {
    height: 50,
    width: 250,
    borderColor: 'red', 
    borderBottomWidth: 1,
    },

  Input:{
    height: 40,
    width: 300,
    borderWidth: 2,
  },

  button: {
    backgroundColor: "red",
    height: 40,
    width: 100,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },

  Text: {
    color: "blue"
  },
});

export default app;

