import React from 'react'
import { View, StyleSheet, TextInput, Text, Button, Pressable } from 'react-native';
import ApolloClient from 'apollo-boost';
import { ApolloProvider, Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';

let name, age, email;

export function Register() {

const onPressHandler = () => {
    navigation.navigate('GetProfile');
} 

                return (
                <View style={styles.container}>
                    <Text>Get Started</Text>
                    <TextInput ref={ value => name = value} id="name" placeholder={'UserName'} style={styles.InputStyles}/>
                    <TextInput ref={ value => email = value} id="email" placeholder={'Email'} style={styles.InputStyles}/>  
                    <TextInput ref={ value => age = value} id="age" placeholder={'Age'} style={styles.InputStyles}/>
        
                    <Button style={styles.Button} 
                        title="Submit" onsubmit={e => {
                        e.preventDefault();
                        createCustomer( {variables: {name: name.value, age: age.value, email: email.value }});                    
                    }}/>
                    <Pressable
                        onPress={onPressHandler}
                    >
                        <Text> page2 </Text>
                    </Pressable>
                </View>
                )
    
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },

    InputStyles: {
        height: 60,
        width: 250,
        borderColor: 'red', 
        borderBottomWidth: 1,
        },

    Pressable: {
        height: 200,
        width: 50,
    },

    Button: {
        backgroundColor: "red",
        height: 70,
        width: 100,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
      },
});


